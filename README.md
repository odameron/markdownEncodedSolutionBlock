Generate a markdown block with a base64-encoded version of the string passed as parameter.

# Usage

The `mdEncodeSolution.py` contains a `generateMarkdownSolutionBlock(clearString)` function that returns a fenced block of markdown with the base64-encoded version of `clearString` (and copies it to the clipboard). 
It can also be executed as a standalone program.


```bash
[python3] mdEncodeSolution.py Here is the sentence you would like to hide
```

Generates:

````markdown
```bash
# SOLUTION:
echo "SGVyZSBpcyB0aGUgc2VudGVuY2UgeW91IHdvdWxkIGxpa2UgdG8gaGlkZQ==" | base64 --decode
```
````

Which displays as:

```bash
# SOLUTION:
echo "SGVyZSBpcyB0aGUgc2VudGVuY2UgeW91IHdvdWxkIGxpa2UgdG8gaGlkZQ==" | base64 --decode
```

**NB:** if `clearString` spans multiple lines, it should be quoted:

```bash
[python3] mdEncodeSolution.py "Here is the sentence 
in multiple lines
that you would like to hide
"
```
Generates:

````markdown
```bash
# SOLUTION:
echo "SGVyZSBpcyB0aGUgc2VudGVuY2UgCmluIG11bHRpcGxlIGxpbmVzCnRoYXQgeW91IHdvdWxkIGxpa2UgdG8gaGlkZQo=" | base64 --decode
```
````




# Todo

- [x] ~~possible to copy the result to the clipboard?~~
- [x] ~~escape quotes in text~~


