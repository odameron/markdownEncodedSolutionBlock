#! /usr/bin/env python3

import base64
import pyperclip
import sys


def generateMarkdownSolutionBlock(clearString):
	markdownBlock = """```bash
# SOLUTION:
echo "{}" | base64 --decode
```""".format(base64.b64encode(bytes(clearString, sys.getdefaultencoding())).decode())
	pyperclip.copy(markdownBlock)
	return markdownBlock

if __name__ == "__main__":
	print(generateMarkdownSolutionBlock(' '.join(sys.argv[1:])))
